package dm1d1a.telegrambot;

public class EjemplosClase {


	/*
	 * Devuelve un String con una lista de los primeros "times" números divisibles entre "multipleOf"
	 */
	public static String firstMultiplesOf(int multipleOf, int times) {
		String output = "";
		int counter = 0;
		int number = 1;
		while (counter < times) {
			if (number % multipleOf == 0) {
				output = output + number + ", ";
				counter = counter + 1;
			}
			number = number + 1;
		}
		return output;
	}
	
	/*
	 * Devuelve un String con una lista de las primeras "times" potencias con base "base"
	 */
	public static String firstPowersOf(int base, int times) {
		String output = "Sin emplementar";
		
		// TODO
		
		return output;
	}
	
	/*
	 * Devuelve un String con una lista de los divisores de "number"
	 */
	public static String dividersOf(int number) {
		String output = "Sin emplementar";
		
		// TODO
		
		return output;
	}
	
	/*
	 * Devuelve un true si "number" es un número primo, o false en caso contrario
	 */
	public static boolean isPrime(int number) {
		boolean primo = false;
		
		// TODO
		
		return primo;
	}
	
	/*
	 * Devuelve los "t" primeros números primos
	 */
	public static String firstPrimes(int times) {
		String output = "Sin emplementar";
		
		// TODO
		
		return output;
	}
	
	/*
	 * Devuelve los números primos entre "first" y "last"
	 */
	public static String primesBetween(int first, int last) {
		String output = "Sin emplementar";
		
		// TODO
		
		return output;
	}
	
	/*
	 * Suma los dígitos de "num"
	 */
	public static int addDigits(int num) {
		int sum = -1;
		
		// TODO
		
		return sum;
	}
	

	/*
	 * Cuenta los dígitos de "num"
	 */
	public static int countDigits(int num) {
		int sum = -1;
		
		// TODO
		
		return sum;
	}
	
	/*
	 * Invierte el orden de los dígitos de "num"
	 */
	public static int reverseDigits(int num) {
		int sum = -1;
		
		// TODO
		
		return sum;
	}
	
}
