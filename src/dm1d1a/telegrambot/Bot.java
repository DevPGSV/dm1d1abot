package dm1d1a.telegrambot;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import com.pengrad.telegrambot.model.request.ForceReply;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.GetMe;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.GetMeResponse;

public class Bot extends TelegramBot {
	
	private User botInfo;

	public Bot(String botToken) {
		super(botToken);
		
		GetMeResponse response = this.execute(new GetMe());
        this.botInfo = response.user();
	}
	
	public User getMe() {
    	return this.botInfo;
    }
	
	public void printBotInfo() {
		System.out.println("Bot Started");
    	System.out.println("-----------");
    	System.out.println("username:\t@"+getMe().username());
    	System.out.println("id:\t\t" + getMe().id());
    	System.out.println("type:\t\t" + (getMe().isBot() ? "bot" : "user") );
    	System.out.println("-----------");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bot bot = new Bot("TOKEN");
		bot.printBotInfo();
		
		bot.setUpdatesListener(new UpdatesListener() {
		    @Override
		    public int process(List<Update> updates) {
		    	for(Update update : updates) {
		    		bot.onUpdateReceived(update);
		    	}
		        return UpdatesListener.CONFIRMED_UPDATES_ALL;
		    }
		});
	}
	
	private void onUpdateReceived(Update update) {
		Message msg = update.message();
		if (msg != null) {
			String formattedDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(msg.date());
			System.out.println("["+formattedDate+"][" + msg.from().firstName() + "@"+msg.chat().firstName()+"] " + msg.text());
			onMessageReceived(msg);
		}
		
	}
	
	private void onMessageReceived(Message msg) {
		String answer = "";
		String words[] = msg.text().split(" ");
		String command = words[0];
		if (command.endsWith("@" + getMe().username())) {
			command = command.substring(0, command.lastIndexOf("@" + getMe().username())); // remove bot username if present in command
		}
		
		command = command.toLowerCase();
		
		switch(command) {
			case "/start":
				answer = "Hola, mándame /help para ver la lista de comandos";
				break;
			case "/help":
				answer = "Commandos:\n" +
						 "/ping - Devuelve pong\n" +	
						 "/help - Muestra los comandos\n" +
						 "/firstMultiplesOf <n> <t> - Los <t> primeros múltiplos de <n>\n" +
						 "/firstPowersOf <b> <t> - Las <t> primeras potencias de base <b>\n" +
						 "/dividersOf <n> - Lista los números divisores de <n>\n" +
						 "/isPrime <n> - Calcula si <n> es primo o no\n" +
						 "/firstPrimes <t> - Devuelve los <t> primeros números primos\n" +
						 "/primesBetween <a> <b> - Devuelve los primos entre <a> y <b>\n" +
						 "/addDigits <n> - Suma los dígitos de <n>\n" +
						 "/countDigits <n> - Cuenta los dígitos de <n>\n" +
						 "/reverseDigits <n> - Invierte el orden de los dígitos de <n>\n"
						 ;
				break;
			case "/ping":
				answer = "pong";
				break;
			case "/firstmultiplesof":
				if (words.length == 3) {
					int multipleOf = Integer.parseInt(words[1]);
					int times = Integer.parseInt(words[2]);
					answer = EjemplosClase.firstMultiplesOf(multipleOf, times);
				} else {
					answer = "Uso:\n /firstMultiplesOf 3 10\n Los 10 primeros múltiplos de 3";
				}
				break;
			case "/firstpowersof":
				if (words.length == 3) {
					int base = Integer.parseInt(words[1]);
					int times = Integer.parseInt(words[2]);
					answer = EjemplosClase.firstPowersOf(base, times);
				} else {
					answer = "Uso:\n /firstPowersOf 2 11\n Las 11 primeras potencias de base 2";
				}
				break;
			case "/dividersof":
				if (words.length == 2) {
					int number = Integer.parseInt(words[1]);
					answer = EjemplosClase.dividersOf(number);
				} else {
					answer = "Uso:\n /dividersOf 10\n Lista los números divisores de 10";
				}
				break;
			case "/isprime":
				if (words.length == 2) {
					int number = Integer.parseInt(words[1]);
					answer = (EjemplosClase.isPrime(number) ? "Es primo" : "No es primo");
				} else {
					answer = "Uso:\n /isPrime 19\n Calcula si 19 es primo o no";
				}
				break;
			case "/firstprimes":
				if (words.length == 2) {
					int times = Integer.parseInt(words[1]);
					answer = EjemplosClase.firstPrimes(times);
				} else {
					answer = "Uso:\n /isPrime 19\n Calcula si 19 es primo o no";
				}
				break;
			case "/primesbetween":
				if (words.length == 3) {
					int first = Integer.parseInt(words[1]);
					int last = Integer.parseInt(words[2]);
					answer = EjemplosClase.primesBetween(first, last);
				} else {
					answer = "Uso:\n /isPrime 19\n Calcula si 19 es primo o no";
				}
				break;
			case "/adddigits":
				if (words.length == 2) {
					int num = Integer.parseInt(words[1]);
					answer = String.valueOf(EjemplosClase.addDigits(num));
				} else {
					answer = "Uso:\n /addDigits 123\n Suma los dígitos de 123";
				}
				break;
			case "/countdigits":
				if (words.length == 2) {
					int num = Integer.parseInt(words[1]);
					answer = String.valueOf(EjemplosClase.countDigits(num));
				} else {
					answer = "Uso:\n /countDigits 123\n Cuenta los dígitos de 123";
				}
				break;
			case "/reversedigits":
				if (words.length == 2) {
					int num = Integer.parseInt(words[1]);
					answer = String.valueOf(EjemplosClase.reverseDigits(num));
				} else {
					answer = "Uso:\n /reverseDigits 123\n Invierte el orden de los dígitos de 123";
				}
				break;
			default:
				answer = "You said: " + msg.text();
				break;
		}
		
		if (!answer.isEmpty()) {
			SendMessage request = new SendMessage(msg.chat().id(), answer)
			   //.parseMode(ParseMode.HTML)
			   .disableWebPagePreview(true)
			   .disableNotification(true)
			   .replyToMessageId(msg.messageId())
			   //.replyMarkup(new ForceReply())
			;
	
			execute(request);
		}
		
	}

}
